package stringlengthfinder;

public class StringLengthFinder {
	/**
	 * main method  will print the length of the string.
	 */
	public static void main(String arg[]) {
		int length = StringLengthCalculator("Jenkins assigment");
		System.out.println("length of string entered:" + length);
	}

	/**
	 * Method will calculate the length and return the value.
	 * @param string entered.
	 * @return 0 if string is null or the length of the string.
	 */
	public static int StringLengthCalculator(String string) {
		
        if(string != null) {
        	int stringLength = string.length();
        	return stringLength;
        }
        return 0;
	}
}
