package stringlengthfinder;

import org.junit.Test;

import junit.framework.Assert;

public class StringLengthFinderTest {

	@Test
	public void testIfStringNull() {
		int stringLengthCalculator = StringLengthFinder.StringLengthCalculator(null);
		Assert.assertEquals(0, stringLengthCalculator);
	}
	
	@Test 
	public void testStringLengthFinder() {
		int stringLengthCalculator = StringLengthFinder.StringLengthCalculator("testing");
		Assert.assertEquals(7, stringLengthCalculator);
	}

}
